
## Functional Programming in Kotlin

* Introduction
    * Why Functional Programming
        * Imperative 
    * Kotlin as a FP Language
        * Functions are first class citizens
            * Higher-Order
            * Top-Level
            
* Basics of Functions in Kotlin
    * Single-Expression Functions
    * Higher Order
    * Lambdas
    * Anonymous Functions
* Standard Library Functions
* Immutability
    * Variables
    * Data Classes
    * Properties
    * Collections
* Constructs
    * Composition 
    * Currying
    * Partials
* Data Types
    * ADT
    * Arrow
        * Try
        * Either
        * Option
        * Option2
        * Option3
        * Option4
        * Optics
* Mondas et al
    * Functors
    * Applicatives
    * Monads
    * Binding Arrow
* Optimisation
    * Inline
        * Local Returns
    * Recursion
        * Folds
        * Tailrec
    * Memoisation
    * Lazy Evaluation    
* DSLs’
    * Basics 
        * With Let Apply
    * Lambdas with Receivers
    * Invoke

    