package com.hadihariri.kotlinfp.monads


data class Price(val buy: Double, val sell: Double) {
    operator fun plus(price: Price): Price {
        return Price(this.buy + price.buy, this.sell + price.sell)
    }
}
















































fun main(args: Array<String>) {

    val prices = listOf(Price(200.10, 210.21), Price(32.17, 36.10), Price(570.40, 600.23))




    val sum = prices.reduce({ x, y -> x + y })

    println(sum)




}
