@file:Suppress("unused")
package com.hadihariri.kotlinfp.constructs

import org.intellij.lang.annotations.*


fun maxNumbers(list: List<Int>): Int {
    return when (list.count()) {
        0 -> throw IllegalArgumentException("This list is empty")
        1 -> list[0]
        else -> Math.max(list[0], maxNumbers(tail(list)))

    }
}


fun maxNumbersFold(list: List<Int>): Int {
    return list.fold(0, { x, y -> Math.max(x, y) })
}























