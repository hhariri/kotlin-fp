@file:Suppress("unused")
package com.hadihariri.kotlinfp.constructs

import arrow.core.*
import com.hadihariri.kotlinfp.optimisation.*


fun main(args: Array<String>) {
    val sumCurried = ::sum.curry()
    sumCurried(2)(3)

}

