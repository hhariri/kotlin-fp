@file:Suppress("unused")
package com.hadihariri.kotlinfp.constructs

import arrow.syntax.function.*
import com.hadihariri.kotlinfp.optimisation.*

fun main(args: Array<String>) {
    val sumPartial = ::sum.partially1(2)
    println(sumPartial(3))

}