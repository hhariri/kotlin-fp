package com.hadihariri.kotlinfp.arrow

import arrow.core.*


object NotApplicable

fun getVATValue(country: String): Either<NotApplicable, Double> {

    return if (country != "ES") {
        Either.Left(NotApplicable)
    } else {
        Either.Right(30000.0)
    }
}

fun main(args: Array<String>) {

    println(getVATValue("ES"))
    println(getVATValue("UK"))



}