package com.hadihariri.kotlinfp.arrow

import arrow.core.*


fun divide(x: Int, y: Int): Option<Int> {
    return if (y > 0) {
        Option.just(x / y)
    } else {
        Option.empty()
    }
}


fun main(args: Array<String>) {
    val result = divide(100, 5)
    when (result) {
        is None -> println("This won't work")
        is Some -> println(result.t)
    }
}