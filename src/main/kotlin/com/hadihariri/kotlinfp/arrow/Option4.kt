package com.hadihariri.kotlinfp.arrow

import arrow.core.*


fun main(args: Array<String>) {

    val result = tripleItOption(Option.just(3))

    val output = result.flatMap {
        Option.just(90 / it)
    }

    println(output.getOrElse { "Value was empty" })

}