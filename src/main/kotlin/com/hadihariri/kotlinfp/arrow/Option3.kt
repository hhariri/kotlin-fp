@file:Suppress("unused")
package com.hadihariri.kotlinfp.arrow

import arrow.core.*


fun squareIt(x: Int): Option<Int> {
    return if (x == 0) {
        Option.empty()
    } else {
        Option.just(x * x)
    }
}

fun tripleIt(x: Int): Option<Int> {
    return if (x == 0) {
        Option.empty()
    } else {
        Option.just(x * x * x)
    }
}

fun squareItOption(x: Option<Int>): Option<Int> {
    return x.map { it * it }
}

fun tripleItOption(x: Option<Int>): Option<Int> {
    return x.map { it * it * it }
}

fun main(args: Array<String>) {
    println(tripleItOption(Option.empty()))
}