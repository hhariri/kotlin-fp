package com.hadihariri.kotlinfp.arrow

import arrow.core.*
import arrow.instances.*
import arrow.typeclasses.*


fun main(args: Array<String>) {

    ForTry extensions {
        binding {
            val a = Try { "3".toInt() }.bind()
            val b = Try { "4".toInt() }.bind()
            val c = Try { "5".toInt() }.bind()
            println(a + b + c)
        }
    }

}