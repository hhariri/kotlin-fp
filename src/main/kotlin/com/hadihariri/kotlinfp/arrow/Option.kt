package com.hadihariri.kotlinfp.arrow

import arrow.core.*

fun processRequest(input: String): Option<String> {
    if (input.contains("valid")) {
        return Option("All Good!")
    } else {
        return Option.empty()
    }
}

fun main(args: Array<String>) {
    val result = processRequest("this is valid text")

    if (result.isDefined()) {
        println("There is a value")
    }

}
