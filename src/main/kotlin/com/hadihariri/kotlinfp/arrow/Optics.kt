package com.hadihariri.kotlinfp.arrow

import arrow.optics.*


@optics
data class Telephone(val prefix: String, val number: String) {
    companion object
}

@optics
data class Contact(val email: String, val telephone: Telephone) {
    companion object
}

@optics
data class Customer(val name: String, val contact: Contact) {
    companion object
}


fun main(args: Array<String>) {

    val customer = Customer("Joe", Contact("joe@gmail.com", Telephone("34", "555555555")))
    val number = customer.contact.telephone.number

    println(number)
}