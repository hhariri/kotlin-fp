package com.hadihariri.kotlinfp.optimisation


class Log
class Logger {
    val log : Log by lazy { Log() }
}

fun main(args: Array<String>) {

    generateSequence(1) {
        it * 10
    }.take(5)
            .forEach { println(it) }
}