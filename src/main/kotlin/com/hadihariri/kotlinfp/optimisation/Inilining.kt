@file:Suppress("unused")
package com.hadihariri.kotlinfp.optimisation


// adding inline below changes things
// add params noinline
inline fun operation(x: Int, y: Int, noinline func: (Int, Int) -> Int) {
    println("Function being called")
    func(x,y)
    println("Function was called")

}

fun sum(x: Int, y: Int): Int {
    println("Sum is called")
    val r = x + y
    println("Sum finishes")
    return r
}


fun callingOperation() {
    //operation(1, 2,  ::sum)
    operation(1, 2, { x,y ->
        println("Start")
        x + y
    })
}

fun callOperationAgain() {
    //operation(1, 2, ::sum)
    operation(1, 2, { x,y ->
        println("Start")
        x + y
    })
}


// can't inline

/*fun opStore(func: (Int, Int) -> Int) {
    execute(func)
}

fun execute(func: (Int, Int) -> Int) {

}*/
