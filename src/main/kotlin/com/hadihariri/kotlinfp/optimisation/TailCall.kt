@file:Suppress("unused")
package com.hadihariri.kotlinfp.optimisation


fun factorial(number: Int): Int {
    return when (number) {
        0, 1 -> 1
        else -> number * factorial(number - 1)
    }
}

fun factorialTailCall(number: Int) = factorialTC(number, 1)

tailrec fun factorialTC(number: Int, accumulator: Int) : Int {
    return when (number) {
        0 -> accumulator
        else -> factorialTC(number - 1, accumulator)
    }
}

