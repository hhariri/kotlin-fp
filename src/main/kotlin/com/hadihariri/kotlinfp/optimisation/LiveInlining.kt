package com.hadihariri.kotlinfp.optimisation


fun printHello() {

    val numbers = 1..10
    numbers.forEach(fun(x) {
        if (x % 5 == 0) {
            return
        }
    })
    println("Hello")
}

fun main(args: Array<String>) {

    printHello()
}