package com.hadihariri.kotlinfp.patterns


// Template Patterns
abstract class Record {
    abstract fun edit()
    abstract fun save()

    fun checkPermissions() {
        // Do some checks
    }

    fun modify() {
        checkPermissions()
        edit()
        save()
    }
}



class CustomerRecord : Record() {
    override fun edit() {
    }

    override fun save() {
    }
}

class InvoiceRecord : Record() {
    override fun edit() {
    }

    override fun save() {
    }
}




