package com.hadihariri.kotlinfp.patterns


// Strategy Pattern
interface SortAlgorithm {
    fun <T> sort(list: List<T>): List<T>
}

class QuickSort : SortAlgorithm {
    override fun <T> sort(list: List<T>): List<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class BubbleSort : SortAlgorithm {
    override fun <T> sort(list: List<T>): List<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
