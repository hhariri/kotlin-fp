package com.hadihariri.kotlinfp.patterns

data class Customer(val id: String, val name: String, val type: String, val country: String)