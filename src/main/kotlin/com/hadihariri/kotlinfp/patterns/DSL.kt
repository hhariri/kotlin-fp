package com.hadihariri.kotlinfp.patterns


class Configuration(var host: String = "", var port: Int = 0)

fun main(args: Array<String>) {


    configuration {
        host = "localhost"
        port = 9090
    }

}

fun configuration(function: Configuration.() -> Unit) {

}

