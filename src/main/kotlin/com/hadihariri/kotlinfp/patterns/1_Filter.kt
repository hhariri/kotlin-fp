package com.hadihariri.kotlinfp.patterns


// Filter Pattern
class CustomerFilter {
    fun filterByName(customers: List<Customer>, name: String): List<Customer> {
        val matchedCustomers = arrayListOf<Customer>()
        for (customer in customers) {
            if (customer.name == name) {
                matchedCustomers.add(customer)
            }
        }
        return matchedCustomers
    }

    fun filterByCountry(customers: List<Customer>, country: String): List<Customer> {
        val matchedCustomers = arrayListOf<Customer>()
        for (customer in customers) {
            if (customer.country == country) {
                matchedCustomers.add(customer)
            }
        }
        return matchedCustomers
    }
}

