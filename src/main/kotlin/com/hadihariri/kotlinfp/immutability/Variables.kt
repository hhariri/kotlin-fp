@file:Suppress("unused", "UNUSED_VARIABLE", "ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE")
package com.hadihariri.kotlinfp.immutability


fun main(args: Array<String>) {
    var mutable = "initial value"
    val immutable  = "initial value"
    mutable = "another value"
    // immutable = "cannot assign"
}