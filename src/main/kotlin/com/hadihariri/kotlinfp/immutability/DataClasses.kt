@file:Suppress("unused", "UNUSED_VARIABLE", "ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE")
package com.hadihariri.kotlinfp.immutability



data class PackageUrl(val url: String, val allowsUnsafe: Boolean)
data class PackageRepository(val name: String, val url: PackageUrl)
data class Package(val name: String, val type: String, val latest_version: String, val repository: PackageRepository)

fun main(args: Array<String>) {


    val http = Package("OKHttp", "JVM", "1.2.3", PackageRepository("maven", PackageUrl("https://maven.org", false)))























    val spek = http.copy(name = "Spek", latest_version = "1.4.5", repository = PackageRepository("maven", PackageUrl("https://maven.org", true)))



















    // @optics would help solve this issue
    //https://github.com/arrow-kt/arrow/issues/859

}
