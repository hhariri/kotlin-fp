@file:Suppress("unused")
package com.hadihariri.kotlinfp.refactoring

import com.hadihariri.kotlinfp.Album

// Flattening com.hadihariri.kotlinfp.data in a manual way

fun albumsWithTrackLengthOfV1(durationInSeconds: Int, albums: List<Album>): ArrayList<Pair<String, String>> {
    val list = arrayListOf<Pair<String, String>>()
    albums.forEach {
        val albumTitle = it.title
        it.tracks.filter { it.durationInSeconds < durationInSeconds }.map { Pair(albumTitle, it.title) }
    }
    return list
}












// Flattening using a function

fun albumsWithTrackLengthOfV2(durationInSeconds: Int, albums: List<Album>): List<Pair<String, String>> {
    return albums.flatMap {
        val albumTitle = it.title
        it.tracks.filter { it.durationInSeconds < durationInSeconds }.map { Pair(albumTitle, it.title) }
    }
}

